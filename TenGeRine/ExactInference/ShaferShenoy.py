#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 15:54:40 2020

@author: gaspard
"""

import time

from TenGeRine.ExactInference.ExactInference import ExactInference

class ShaferShenoy(ExactInference):
    r"""
    Class used for ShaferShenoy inference.

    Parameters
    ----------
    bn : pyAgrum.BayesNet
        a Bayesian network
    root (opt) : int
        the id of a root in the junction tree
    evidence (opt) : dict
        a dict of evidence
    info (opt) : boolean
        print inference times
    full (opt) : boolean
        compute posteriors from every cliques
    target (opt) : str
        the name of a variable to be targeted
    """

    def __init__(self,bn,root=None,evidence=dict(),target=None):
        super().__init__(bn,root,evidence,target)

    def makeInference(self,full=False,withDetails=False,info=False):
        r"""
        makeInference(ShaferShenoy self) -> dict, list, int

        Perform the heavy computations needed to compute the targets' posteriors

        Returns
        -------
        dico_marginales : dict
            a dict with posteriors for each node
        dict
            a dictionnary contaning computation times of the different phases in inference
        int
            the number of parameters in the final potentials
        """

        t_pretreat = 0

        if info:
            print("Initialisation")
            print('--------------')
        t0 = time.time()
        super()._initialisation()
        t_init = time.time()-t0

        connectedComponents = self.jt.connectedComponents()
        roots = []
        t_collect = []
        t_distrib = []

        if len(connectedComponents)==1 and self.root!=None:
            roots = [self.root]
        else:
            for elt in connectedComponents:
                roots.append(list(connectedComponents[elt])[0])

        for root in roots:
            if info:
                print("=> ok : durée %.2f" % t_init)
                print()

            if info:
                print("Phase de collecte")
                print('-----------------')
                
            t0 = time.time()
            self.__collect(root,root)
            collect = time.time()-t0
            t_collect.append(collect)

            if info:
                print("=> ok : durée %.2f" % collect)
                print()

            if info:
                print("Phase de distribution")
                print('---------------------')
                
            t0 = time.time()
            self.__distrib(root,root)
            distrib = time.time()-t0
            t_distrib.append(distrib)
            
            if info:
                print("=> ok : durée %.2f" % distrib)
                print()
                
        if info:
            print("Mise à jour des potentiels")
            print('--------------------------')
        
        t0 = time.time()
        super()._updatePotentials()
        t_pot_update = time.time()-t0

        if info:
            print("=> ok : durée %.2f" % t_pot_update)
            print()
            
        if info:
            print("Calcul des marginales")
            print('---------------------')
            
        t0 = time.time() 
        
        if full:
            dico_marginales = super().getMarginalesFull()
        else:
            dico_marginales = super().getMarginales()
            
        t_marginales = time.time()-t0
        
        if info:
            print("=> ok : durée %.2f" % t_marginales)
            print()

        self.marginales = dico_marginales

        if withDetails:
            nb_param = 0

            for elt in self.phi:
                r = 1
                for e in self.phi[elt].var_dims:
                    r = r*e
                nb_param+= r
                
            for elt in self.psi:
                r = 1
                for e in self.psi[elt].var_dims:
                    r = r*e
                nb_param+= r
                
            times = {   'Pre treatment':t_pretreat,
                        'Initialization': t_init,
                        'Collection phase':sum(t_collect),
                        'Distribution phase':sum(t_distrib),
                        'Potential update':t_pot_update,
                        'Posterior generation':t_marginales}
            return dico_marginales, times, nb_param
        else :
            return 

    def probabilityOfEvidence(self,withDetails=False,info=False):
        r"""
        probabilityOfEvidence(ShaferShenoyTensorTrain self) -> list, list, int

        Perform the heavy computations needed to compute the root posterior (collect phase of SS)

        WARNING : the root must be chosen before the inference

        Returns
        -------
        potential_root : list
            the posterior associated with the root after the collect phase
        probabilityOfEvidence : float
        list
            a list contaning computation times of the different phases in inference
        """

        t_pretreat = 0
        if info:
            print("Initialisation")
            print('--------------')
        t0 = time.time()
        super()._initialisation()
        t_init = time.time()-t0

        t_collect = []

        # Looking for a clique if root is undefined
        if self.root == None:
            for clique in self.jt.nodes():
                if self.target in self.jt.clique(clique):
                    self.root = clique
                    break
                    
        if info:
            print("=> ok : durée %.2f" % t_init)
            print()

        if info:
            print("Phase de collecte")
            print('-----------------')
            
        t0 = time.time()
        self.__collect(self.root,self.root)
        collect = time.time()-t0
        t_collect.append(collect)

        if info:
            print("Mise à jour des potentiels")
            print('--------------------------')
        
        potential_root = self.phi[self.root]

        for key in self.psi:
            if key[1]==self.root:
                potential_root = potential_root * self.psi[key]

        probabilityOfEvidence = potential_root.sum()

        if withDetails:
            times = {   'Pre treatment':t_pretreat,
                        'Initialization': t_init,
                        'Collection phase':sum(t_collect)}
            return potential_root, probabilityOfEvidence, times
        else :
            return probabilityOfEvidence

    def __collect(self,i,j):
        r"""
        __collect(ShaferShenoy self, int i, int j)

        Computes the message going from i to j

        Parameters
        ----------
        i : int
            a clique id
        j : int
            a clique id
        """
        phi_i = self.phi[i]
        
        for key in self.psi:
            if key[0] == i and key[1] != j:
                k = key[1]
                self.__collect(k,i)
                phi_i = phi_i * self.psi[k,i]
        
        if i!=j:
            to_marg_out = list(set(self.cli_desc[i])-set(self.psi[i,j].var_names))
            psi_i_j = phi_i.margSumOut(to_marg_out)
            self.psi[i,j] = psi_i_j

    def __distrib(self,i,j):
        r"""
        __distrib(ShaferShenoy self, int i, int j)

        Computes the message going from i to j

        Parameters
        ----------
        i : int
            a clique id
        j : int
            a clique id
        """
        phi_i = self.phi[i]
        for key in self.psi:
            if key[1]==i and key[0]!=j :
                k = key[0]
                phi_i = phi_i * self.psi[k,i]
    
        if i!=j:
            to_marg_out = list(set(self.cli_desc[i])-set(self.psi[i,j].var_names))
            psi_i_j = phi_i.margSumOut(to_marg_out)
            self.psi[i,j] = psi_i_j
    
        for key in self.psi:
            j_k = key
            if j_k[0]==j:
                k = j_k[1]
                if k!=i:
                    self.__distrib(j,k)

    def posterior(self,name):
        for desc in self.cli_desc:
            if name in self.cli_desc[desc]:
                return self.phi[desc].margSumIn(name).normalize()
                