#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 16:23:32 2020

@author: gaspard
"""

import pyAgrum as gum

from TenGeRine.JTBasedInference import JTBasedInference
class ExactInference(JTBasedInference):

    def __init__(self, bn, root=None, evidence=dict(),target=None):
        super().__init__(bn,root,evidence,target)
        
    def _initialisation(self):
        r"""
        _initialisation(ExactInference self)

        Computes the inital posteriors of cliques and separators 
        """
        listNodes = self.bn.nodes()

        for clique in self.jt.nodes():

            tmp = gum.Potential()
            for node in self.jt.clique(clique):
                if node in listNodes:
                    isCompatible = True
                    cpt_node = self.bn.cpt(node)
                    var_names = cpt_node.var_names

                    for elt in var_names:
                        a_list = list(self.jt.clique(clique))
                        var_list = [self.bn.variable(var).name() for var in a_list]
                        if not(elt in var_list):
                            isCompatible = False
                    if isCompatible:

                        variable = self.bn.variable(node).name()
                        if variable in self.evidence:
                            tmp = tmp*cpt_node*self.evidence[variable]
                        else:
                            tmp=tmp*cpt_node
                        listNodes.remove(node)

            for node in self.jt.clique(clique):
                if not(self.bn.variable(node).name() in tmp.var_names):

                    tmp = tmp * gum.Potential().add(self.bn.variable(node)).fillWith(1)

            self.phi[clique] = tmp
            self.cli_desc[clique] = tmp.var_names

        for edge in self.jt.edges():
            tmp = gum.Potential()
            list_names = []
            id_set_0 = self.jt.clique(edge[0])
            id_set_1 = self.jt.clique(edge[1])
            id_set = id_set_0.intersection(id_set_1)  

            list_names = [self.bn.variable(var).name() for var in id_set]   
            for var in list_names:
                tmp=tmp.add(self.bn.variable(var))
            tmp.fillWith(1)

            self.psi[edge[0],edge[1]] = tmp
            self.psi[edge[1],edge[0]] = tmp
            
    def _updatePotentials(self):
        r"""
        _updatePotentials(ExactInference self)

        Computes the value of each clique's potential regarding its incoming messages
        """
        for j in self.phi:
            potential = self.phi[j]
            for key in self.psi:
                if key[1]==j:
                    potential = potential * self.psi[key]
            self.phi[j] = potential
    
    def getProbabilityOfEvidence(self):
        r"""
        Inference computes P(x,e) for all x. 
        Therefore, before normalizing, the probability of evidence can be compute 
        by summing out any potential (if the BN is connex)

        Returns
        -------
        pev: float
          the probability of evidence
        """
        for clique in self.phi:
          #the first clique is the good one (providing the Bayesnet if connex)
          return self.phi[clique].sum()

        # no clique, no ev, no power, no responability
        return 1.0

    def getMarginales(self):
        r"""
        getMarginales(ExactInference self)

        Returns
        -------
        dico_marginales : dict
            a dictionnary contaning a posterior of each node
        """
        dico_marginales = dict()
    
        for node in self.bn.nodes():
            name = self.bn.variable(node).name() 
            for clique in self.phi:
                var_names = self.cli_desc[clique]
                if name in var_names:
                    dico_marginales[name] = [self.phi[clique].margSumIn(name).normalize()[:]]
                    break
        return dico_marginales
    
    def getMarginalesFull(self):
        r"""
        getMarginalesFull(ExactInference self)

        Returns
        -------
        dico_marginales : dict
            a dictionnary contaning the posteriors of each node
        """
        dico_marginales = dict()
        
        for node in self.bn.nodes():
            name = self.bn.variable(node).name()
            dico_marginales[name] = []
            
            for i, clique in enumerate(self.phi):
                var_names = self.cli_desc[clique]
    
                if name in var_names:
                    dico_marginales[name].append(self.phi[clique].margSumIn(name).normalize()[:])
    
        return dico_marginales