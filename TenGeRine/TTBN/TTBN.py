import pyAgrum as gum
import numpy as np
from TenGeRine.TensorTrainInference.Tools import trainsform

class TTBN(gum.BayesNet):
    def __init__(self,bn,evidence=dict(),max_tt_rank=np.inf,precision=None):
        super().__init__(bn)

        self.max_tt_rank = max_tt_rank
        self.precision = precision
        self.TTcpt = dict()
        self.TTdesc = dict()
        self.evidence = evidence
        self.BN2TTBN()

    def BN2TTBN(self):
        for node in self.nodes():
            var_name = self.variable(node).name()
            
            new_order = [self.variable(var).name() for var in self.topologicalOrder() if self.variable(var).name() in self.cpt(var_name).var_names]

            if var_name in self.evidence.keys():
                pot = self.cpt(var_name)* (gum.Potential().add(self.variable(var_name)).fillWith(self.evidence[var_name][:]))
            else:
                pot = self.cpt(var_name)

            pot = pot.reorganize(list(reversed(new_order)))

            self.TTdesc[var_name] = new_order
            self.TTcpt[var_name] = trainsform(pot,self.max_tt_rank,self.precision)