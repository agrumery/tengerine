import numpy as np
import tensap

def intersection(lst1, lst2): 
    r"""
    intersection(liste lst1,liste lst2)

    Returns
    -------
    list
        the intersection of lst1 and list2
    """
    lst3 = [value for value in lst1 if value in lst2] 
    return lst3

def marginalized(base_id,to_keep,tt,max_tt_rank,precision):
    r"""
    marginalized(list base_id, list to_keep , t3f.TensorTrain tt, int max_tt_rank, float precision)
    
    Marginalize a tensor train regarding to a list of variables

    Parameters
    ----------
    base_id : list
        the original variables in the tensor train
    to_keep : list
        the variables to keep
    tt : t3f.TensorTrain
        the original tensor train
    max_tt_rank : int
        the maximum rank in the approximation of the marginalized tensor train
    precision : float
        the precision of the approximation of the marginalized tensor train

    Returns
    -------
    t3f.TensorTrain
        a Tensor Train containing only the variables in parameters
    """
    to_marg_out = []
    
    for val in base_id:
        if not(val in to_keep):
            to_marg_out.append(val)

    return margSumOut(to_marg_out,tt,base_id,max_tt_rank,precision)

def trainsform(potential,max_rank=None,precision=1e-3):
    r"""
    trainsform(Potential potential, int max_rank, float precision)

    Approximate a potential with a tensor train

    Parameters
    ----------
    gum.Potential : potential
        the original variables in the tensor train
    max_rank : int
        the maximum rank in the approximation of the tensor train
    precision : float
        the precision of the approximation of the tensor train

    Returns
    -------
    t3f.TensorTrain
        a Tensor Train

    """

    ndarray = potential[:]
    U = tensap.FullTensor(ndarray)

    if U.order == 1 :
        return U

    TR = tensap.Truncator()
    TR.tolerance = precision

    return TR.ttsvd(U)

def cli_sep_product(ttbn,cli,cli_desc,sep,sep_desc,max_tt_rank,precision,reduce=True):
    r"""
    tt_product(TensorTrain tt_a, TensorTrain tt_b, int max_tt_rank, float precision, boolean reduce)

    Compute the elementwise product of two tensor trains

    Parameters
    ----------
    TensorTrain : tt_a
        a tensor train
    TensorTrain : tt_b
        a tensor train
    max_tt_rank : int
        the maximum rank in the approximation of the resulting tensor train
    precision : float
        the precision of the approximation of the resulting tensor train
    reduce : boolean
        indicates if the resulting tensor train should be reduced

    Returns
    -------
    t3f.TensorTrain
        a Tensor Train

    """

    if len(cli_desc) != len(sep_desc):
        updated_sep = update_potential(ttbn,sep,sep_desc,cli_desc)
    else:
        updated_sep = sep

    tt = tt_hadamard_product(cli,updated_sep)

    if reduce:

        TR = tensap.Truncator()
        TR.tolerance = precision

        tensor = TR.ttsvd(tt)

        return tensor
    return tt

def margSumIn(variables,tenseur,tt_desc,max_tt_rank,precision):
    if type(variables)!= list:
        variables = [variables]
    new_idx = toMargOut(variables,tt_desc)
    res = margSumOut(new_idx,tenseur,tt_desc,max_tt_rank,precision)
    return res

def toMargOut(la,lb):
    lc = []
    for e in lb:
        if not(e in la):
            lc.append(e)
    return lc

def margSumOut(variables,tenseur,tt_desc,max_tt_rank,precision):

    actives_nodes = tenseur.active_nodes
    actives_nodes = np.flip(actives_nodes)
    cores = []

    for i in actives_nodes:
        cores.append(tenseur.tensors[i-1])

    if type(variables)==list:
        idx = []
        
        for var in list(reversed(tt_desc)):
            if var in variables:
                idx.append(var)
        
        for elt in idx:
            tenseur = margSumOut(elt,tenseur,tt_desc,max_tt_rank,precision)
        
        return tenseur
    else:
        idx = tt_desc.index(variables)
        
        t = cores[idx]

        if idx == 0:
            if idx == len(cores)-1:
                return tensap.TreeBasedTensor.tensor_train([t])
            else:
                if t.order == 3:
                    t = tensap.FullTensor(np.sum(t.data, 1, keepdims=False))
                cores[idx+1] = t.tensordot(cores[idx+1], [1], [0])
                cores[idx+1] = tensap.FullTensor(np.sum(cores[idx+1].data, 0, keepdims=False))
                cores = [elt for i, elt in enumerate(cores) if i!=idx]
        elif idx == len(cores)-1:
            if idx == 1:
                cores[idx-1] = cores[idx-1].tensordot(t, [1], [0])
                cores[idx-1] = tensap.FullTensor(np.sum(cores[idx-1].data, 1, keepdims=False))
            else:
                cores[idx-1] = cores[idx-1].tensordot(t, [2], [0])
                cores[idx-1] = tensap.FullTensor(np.sum(cores[idx-1].data, 2, keepdims=False))
            
            cores = [elt for i, elt in enumerate(cores) if i!=idx]
        else:
            t = tensap.FullTensor(np.sum(t.data,1,keepdims=False))
            cores[idx+1] = t.tensordot(cores[idx+1], [1], [0])
            cores = [elt for i, elt in enumerate(cores) if i!=idx]

    tt = tensap.TreeBasedTensor.tensor_train(cores)

    return tt

def compress(what,max_rank=None,precision=0):
    if precision == 0:
        return what
    else:
        TR = tensap.Truncator()
        TR.tolerance = precision
        
        tensor = TR.ttsvd(what)

        return tensor

def tt_hadamard_product(cli,sep):
        '''
        Compute the Hadamard product of two TT.

        Parameters
        ----------
        arg : TreeBasedTensor
            The second tensor of the Hadamard product.

        Returns
        -------
        tensap.FullTensor
            The tensor resulting from the Hadamard product.

        '''

        output = []
        
        cli_active_nodes = [val-1 for val in cli.active_nodes]
        cli_active_nodes.reverse()

        first = cli_active_nodes[0]
        last = cli_active_nodes[-1]

        for i, val in enumerate(sep.active_nodes):
            assert val == cli.active_nodes[i]

        for indice in cli_active_nodes:
            cli_core = cli.tensors[indice].data
            sep_core = sep.tensors[indice].data

            if indice == first:
                sep_rank = cli_core.shape[1] * sep_core.shape[1]
                res = np.einsum('ia,ib->iab',cli_core,sep_core)
                output.append(np.reshape(res,[cli_core.shape[0],sep_rank]))
                
            elif indice == last:
                cli_rank = cli_core.shape[0] * sep_core.shape[0]
                res = np.einsum('ai,bi->abi',cli_core,sep_core)
                output.append(np.reshape(res,[cli_rank,cli_core.shape[1]]))
                
            else :
                cli_rank = cli_core.shape[0] * sep_core.shape[0]
                sep_rank = cli_core.shape[2] * sep_core.shape[2]
            
                res = np.einsum('aib,cid->acibd',cli_core,sep_core)
                output.append(np.reshape(res,[cli_rank,cli_core.shape[1],sep_rank]))
                
        return tensap.TreeBasedTensor.tensor_train(output)
    
def add_variable(tensor_train_cores,pos,dim):
    updated_tensor_train_cores = tuple()

    if pos == 0:
        new_tens = new_tensor(1,dim,1).data
        new_tens = np.squeeze(new_tens,axis=0)
        new_tens = tensap.FullTensor(new_tens)

        if len(tensor_train_cores)==1:
            old_head = tensap.FullTensor(np.expand_dims(tensor_train_cores[0].data,axis=0))
            updated_tensor_train_cores = (new_tens,old_head)
        else:
            old_head = tensor_train_cores[0]         
            old_head = np.expand_dims(old_head.data,axis=0)
            old_head = tensap.FullTensor(old_head)
            updated_tensor_train_cores = (new_tens,old_head,*tensor_train_cores[1:])

    elif pos == len(tensor_train_cores):
        new_tens = new_tensor(1,dim,1).data
        new_tens = np.squeeze(new_tens,axis=2)
        new_tens = tensap.FullTensor(new_tens)

        if len(tensor_train_cores)==1:
            old_tail = tensap.FullTensor(np.expand_dims(tensor_train_cores[0].data,axis=1))
            updated_tensor_train_cores = (old_tail,new_tens)
        else:
            old_tail = tensor_train_cores[-1]
            old_tail = np.expand_dims(old_tail.data,axis=2)
            old_tail = tensap.FullTensor(old_tail)
            updated_tensor_train_cores = (*tensor_train_cores[:len(tensor_train_cores)-1],old_tail,new_tens)
    else:
        previous_tensor = tensor_train_cores[pos-1]
        next_tensor = tensor_train_cores[pos]
        left_dim = previous_tensor.size[-1]
        right_dim = next_tensor.size[0]

        updated_tensor_train_cores = (*tensor_train_cores[:pos],new_tensor(left_dim,dim,right_dim),*tensor_train_cores[pos:])

    return updated_tensor_train_cores

def update_potential(bn,tensor_train,current_desc,expected_desc):
    new_cores = []

    if isinstance(tensor_train,tensap.TreeBasedTensor) and tensor_train.order == 1:
        tensor_train = tensor_train.tensors[0]
    
    if isinstance(tensor_train,tensap.FullTensor):
        data = tensor_train.data
        new_cores.append(tensor_train)
        
        for i, elt in enumerate(expected_desc):
            if not(elt in current_desc):
                new_cores = add_variable(new_cores,i,bn.variable(elt).domainSize())
                tensor_train_desc = [*current_desc[:i],elt,*current_desc[i:]]
    else:
        active_nodes = tensor_train.active_nodes
        active_nodes = np.flip(active_nodes).tolist()

        for i in active_nodes:
            new_cores.append(tensor_train.tensors[i-1]) 

        assert len(current_desc) == len(new_cores)

        for i, elt in enumerate(expected_desc):
            if not(elt in current_desc):
                new_cores = add_variable(new_cores,i,bn.variable(elt).domainSize())
                tensor_train_desc = [*current_desc[:i],elt,*current_desc[i:]]   

    res = tensap.TreeBasedTensor.tensor_train(new_cores)

    return res

def tt_identity(tt):

    if isinstance(tt,tensap.FullTensor):
        return tensap.FullTensor(np.array(tt.data,copy=True))

    actives_nodes = tt.active_nodes
    actives_nodes = np.flip(actives_nodes)
    cores = []

    for i in actives_nodes:
        cores.append(tensap.FullTensor(np.array(tt.tensors[i-1].data,copy=True)))

    return tensap.TreeBasedTensor.tensor_train(cores)

def new_tensor(ldim,dim,rdim):

    array = np.zeros((ldim,dim,rdim))

    for i, elt in enumerate(array):
        for j, elt2 in enumerate(elt):
            for k, elt3 in enumerate(elt2):
                if k==i:
                    tmp = elt2
                    tmp[k] = 1
                    array[i,j]=tmp       
    return tensap.FullTensor(array)