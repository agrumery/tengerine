#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 15:54:40 2020

@author: gaspard
"""

import pyAgrum as gum
import tensap
import time
import numpy as np
import math

from TenGeRine.TensorTrainInference.Tools import intersection, update_potential, new_tensor, cli_sep_product, marginalized, margSumIn
from TenGeRine.JTBasedInference import JTBasedInference

from TenGeRine.TTBN.TTBN import TTBN

class ShaferShenoyTensorTrain(JTBasedInference):
    r"""
    Class used for ShaferShenoyTensorTrain inference.

    Parameters
    ----------
    bn : pyAgrum.BayesNet
        a Bayesian network
    max_tt_rank (opt) : int
        maximum rank in the approximation of tensor trains
    precision (opt) : float
        precision of the approximation of tensor trains
    root (opt) : int
        the id of a root in the junction tree
    evidence (opt) : dict
        a dict of evidence
    info (opt) : boolean
        print inference times
    full (opt) : boolean
        compute posteriors from every cliques
    """
    
    def __init__(self,bn,root=None,evidence=dict(),max_tt_rank=np.inf,precision=None,target=None):
        super().__init__(bn,root,evidence,target)
        gen = gum.JunctionTreeGenerator()
        
        self.max_tt_rank = max_tt_rank
        self.precision = precision
        self.ttbn = TTBN(bn,evidence=self.evidence,max_tt_rank=max_tt_rank,precision=precision)
        self.jt = gen.junctionTree(self.ttbn)

    def makeInference(self,full=False,withDetails=False,info=False):
        r"""
        makeInference(ShaferShenoyTensorTrain self) -> dict, list, int

        Perform the heavy computations needed to compute the targets' posteriors

        Returns
        -------
        dico_marginales : dict
            a dict with posteriors for each node
        dict
            a dictionary contaning computation times of the different phases in inference
        int
            the number of parameters in the final potentials
        """
        t_pretreat = 0
                
        self.ttbn = TTBN(self.bn,self.evidence,self.max_tt_rank,self.precision)
        ie = gum.ShaferShenoyInference(self.ttbn)
        self.jt = ie.junctionTree()
            
        if info:
            print("Initialisation")
            print('--------------')
            
        t0 = time.time()
        self._initialisation()
        t_init = time.time()-t0
        
        if info:
            print("=> ok : durée %.2f" % t_init)
            print()
    
        connectedComponents = self.jt.connectedComponents()
    
        roots = []
        t_collect = []
        t_distrib = []
    
        if len(connectedComponents)==1 and self.root!=None:
            roots = [self.root]
        else:
            for elt in connectedComponents:
                roots.append(list(connectedComponents[elt])[0])
    
        for root in roots:
            
            if info:
                print("Phase de collecte")
                print('-----------------')
            t0 = time.time()
            self._collect(root,root)
            collect = time.time()-t0
            t_collect.append(collect)
            if info:
                print("=> ok : durée %.2f" % collect)
                print()
            
            if info:
                print("Phase de distribution")
                print('---------------------')
            t0 = time.time()
            self._distribute(root,root)
            distrib = time.time()-t0
            t_distrib.append(distrib)
            if info:
                print("=> ok : durée %.2f" % distrib)
                print()
        
        if info:
            print("Mise à jour des potentiels")
            print('--------------------------')
        
        t0 = time.time()
        self._updatePotentials()
        t_pot_update = time.time()-t0
    
        if info:
            print("=> ok : durée %.2f" % t_pot_update)
            print()
    
        if info:
            print("Calcul des marginales")
            print('---------------------')
        t0 = time.time()

        if full:
            marginales = self.getMarginalesFull()
        else:
            marginales = self.getMarginales()
    
        t_marginales = time.time()-t0
        
        if info:
            print("=> ok : durée %.2f" % t_marginales)
            print()
        
        self.marginales = marginales

        if withDetails:
            nb_param = 0

            for elt in self.phi:
                nb_param += self.phi[elt].storage()
            for elt in self.psi:
                nb_param += self.psi[elt].storage()

            times = {   'Pre treatment':t_pretreat,
                        'Initialization': t_init,
                        'Collection phase':sum(t_collect),
                        'Distribution phase':sum(t_distrib),
                        'Potential update':t_pot_update,
                        'Posterior generation':t_marginales}
            return marginales, times, nb_param
        else :
            return 

    def probabilityOfEvidence(self,withDetails=False,info=False):
        r"""
        probabilityOfEvidence(ShaferShenoyTensorTrain self) -> list, list, int

        Perform the heavy computations needed to compute the root posterior (collect phase of SS)

        WARNING : the root must be chosen before the inference

        Returns
        -------
        potential_root : list
            the posterior associated with the root after the collect phase
        probabilityOfEvidence : float
        list
            a list contaning computation times of the different phases in inference
        """

        self.ttbn = TTBN(self.bn,self.evidence,self.max_tt_rank,self.precision)
        ie = gum.ShaferShenoyInference(self.ttbn)
        self.jt = ie.junctionTree()

        if self.target == None and self.root == None:
            raise ValueError("A root (or at least a target) is needed for this inference")
            
        if info:
            print("Initialisation")
            print('--------------')
            
        t0 = time.time()
        self._initialisation()
        t_init = time.time()-t0
        
        if info:
            print("=> ok : durée %.2f" % t_init)
            print()
    
        t_collect = []
    
        # Looking for a clique if root is undefined
        if self.root == None:
            for clique in self.jt.nodes():
                if self.target in self.jt.clique(clique):
                    self.root = clique
                    break
                    
        if info:
            print("Phase de collecte")
            print('-----------------')
        t0 = time.time()

        self._collect(self.root,self.root)
        collect = time.time()-t0
        t_collect.append(collect)

        if info:
            print("=> ok : durée %.2f" % collect)
            print()
        
        potential_root = self.phi[self.root]
        for key in self.psi:
            if key[1]==self.root:
                potential_root = cli_sep_product(self.ttbn,potential_root,self.cli_desc[self.root],self.psi[key],self.sep_desc[key],self.max_tt_rank,self.precision)

        probabilityOfEvidence = potential_root.full().data.sum()

        if withDetails:
            times = {'Initialization': t_init,'Collection phase':sum(t_collect)}
            return potential_root, probabilityOfEvidence, times
        else :
            return probabilityOfEvidence
    
    def _initialisation(self):
        r"""
        _initialisation(ShaferShenoyTensorTrain self)

        Computes the inital tensor trains of cliques and separators 
        """
        nodes_list = [self.ttbn.variable(var).name() for var in self.ttbn.topologicalOrder()]

        for clique in self.jt.nodes():
            
            potentials = []
            potentials_desc = []
            topological_order = []
            
            potentials_to_add = dict()

            i = 0
                           
            topological_order = [self.ttbn.variable(var).name() for var in self.ttbn.topologicalOrder() if var in self.jt.clique(clique)]

            for node in topological_order:
                if node in nodes_list:
                    isCompatible = True
    
                    var_in_cpt = self.ttbn.TTdesc[node]
                    
                    for elt in var_in_cpt:
                        if not(elt in topological_order):
                            isCompatible = False
     
                    if isCompatible:
                        
                        potentials.append(self.ttbn.TTcpt[node])

                        potentials_to_add[i] = var_in_cpt
                        potentials_desc.append(node)
                        i+=1
                        
                        nodes_list.remove(node)

            order = topological_order
            new_order = intersection(order,potentials_desc)

            if len(new_order)==0:
                tensors = tuple()
                for i, elt in enumerate(order):
                    if i == 0:
                        tensor = new_tensor(1,self.ttbn.variable(elt).domainSize(),1)
                        tensor = tensap.FullTensor(np.squeeze(tensor.data,axis=0))
                    elif i == len(order)-1:
                        tensor = new_tensor(1,self.ttbn.variable(elt).domainSize(),1)
                        tensor = tensap.FullTensor(np.squeeze(tensor.data,axis=2))
                    else:
                        tensor = new_tensor(1,self.ttbn.variable(elt).domainSize(),1)
                    tensors = (*tensors,tensor)

                tt = tensap.TreeBasedTensor.tensor_train(tensors)
                self.phi[clique] = tt
                self.cli_desc[clique] = order
            else:
                self.phi[clique] = self._construct_clique(order,potentials,potentials_to_add)
                self.cli_desc[clique] = order

        for edge in self.jt.edges():
            list_names = []
            id_set_0 = self.jt.clique(edge[0])
            id_set_1 = self.jt.clique(edge[1])
            id_set = id_set_0.intersection(id_set_1) 

            list_names = [self.ttbn.variable(var).name() for var in self.ttbn.topologicalOrder() if var in id_set]
                    
            self.sep_desc[edge[0],edge[1]]=list_names
            self.sep_desc[edge[1],edge[0]]=list_names

    def _construct_clique(self,cli_desc,tt_potentials,tt_potentials_desc):
        r"""
        _constructTrainFromPotentials(ShaferShenoyTensorTrain self, list clique_var_names, list liste_potentials, list potentials_to_add)

        Constructs a tensor train filled with the product of potentials

        Parameters
        ----------
        cli_desc : list
            the list of variables contained in the clique
        tt_potentials : list
            the list of potentials to add in the tensor train
        tt_potentials_desc : list
            a list containing the variables in each potentials

        Returns
        -------
        t3f.TensorTrain
            a tensor train

        """
        
        cli = update_potential(self.ttbn,tt_potentials[0],tt_potentials_desc[0],cli_desc)

        for i, elt in enumerate(tt_potentials[1:]):
            cli = cli_sep_product(self.ttbn,cli,cli_desc,elt,tt_potentials_desc[i+1],self.max_tt_rank,self.precision)

        return cli
    
    def _collect(self,i,j):
        phi_i = self.phi[i]
        
        for key in self.sep_desc:
            if key[0]==i and key[1]!= j:
                k = key[1]
                self._collect(k,i)
                phi_i = cli_sep_product(self.ttbn,phi_i,self.cli_desc[i],self.psi[k,i],self.sep_desc[k,i],self.max_tt_rank,self.precision)
        if i!=j:
            self.psi[i,j] = marginalized(self.cli_desc[i],self.sep_desc[i,j],phi_i,self.max_tt_rank,self.precision)
    
    def _distribute(self,i,j):
        phi_i = self.phi[i]
        for key in self.sep_desc:
            if key[1]==i and key[0]!=j :
                k = key[0]
                phi_i = cli_sep_product(self.ttbn,phi_i,self.cli_desc[i],self.psi[k,i],self.sep_desc[k,i],self.max_tt_rank,self.precision)

        if i!=j:
            self.psi[i,j] = marginalized(self.cli_desc[i],self.sep_desc[i,j],phi_i,self.max_tt_rank,self.precision)
        for key in self.sep_desc:
            j_k = key
            if j_k[0]==j:
                k = j_k[1]
                if k!=i:
                    self._distribute(j,k)
    
    def _updatePotentials(self):
        for j in self.phi:
            cli = self.phi[j]
            for key in self.psi:
                if key[1]==j:
                    cli = cli_sep_product(self.ttbn,cli,self.cli_desc[j],self.psi[key],self.sep_desc[key],self.max_tt_rank,self.precision)
            self.phi[j] = cli
    
    def getMarginales(self):
        dico_marginales = dict()
    
        for node in self.ttbn.nodes():
            name = self.ttbn.variable(node).name()
            dico_marginales[name] = []
                
            for ide in self.cli_desc:
                if name in self.cli_desc[ide]:
                    potential = gum.Potential().add(self.ttbn.variable(node))
                    tenseur = margSumIn(name,self.phi[ide],self.cli_desc[ide],self.max_tt_rank,self.precision)
                    potential[:] = tenseur.full().data
                    normalized_pot = potential[:]

                    if min(normalized_pot) < 0:
                        new_pot = [normalized_pot[i]+abs(min(normalized_pot)) for i in range(len(normalized_pot))]
                        potential[:] = new_pot

                    normalized_pot = potential.normalize()[:]

                    dico_marginales[name].append(normalized_pot)

                    break
        return dico_marginales

    def getMarginalesFull(self):
        dico_marginales = dict()

        for node in self.ttbn.nodes():
            name = self.ttbn.variable(node).name()
            dico_marginales[name] = []
    
            for ide in self.cli_desc:
                if name in self.cli_desc[ide]:
                    potential = gum.Potential().add(self.ttbn.variable(node))
                    tenseur = margSumIn(name,self.phi[ide],self.cli_desc[ide],self.max_tt_rank,self.precision)
                    potential[:] = tenseur.full().data
                    normalized_pot = potential[:]

                    if min(normalized_pot) < 0:
                        new_pot = [normalized_pot[i]+abs(min(normalized_pot)) for i in range(len(normalized_pot))]
                        potential[:] = new_pot

                    normalized_pot = potential.normalize()[:]

                    dico_marginales[name].append(normalized_pot)
        return dico_marginales

    def getProbabilityOfEvidence(self,with_stats=False):
        r"""
        Inference computes P(x,e) for all x.
        Therefore, before normalizing, the probability of evidence can be compute
        by summing out any potential (if the BN is connex)

        Returns
        -------
        pev: float
        the probability of evidence
        """
        spev = 0.0
        spev2 = 0.0
        nbr = 0
        for ide in self.cli_desc:
            p = self.phi[ide].full().data.sum()
            if not with_stats:
                return p
            spev += p
            spev2 += p * p
            nbr += 1

        mean = spev / nbr
        return mean, math.sqrt(abs(mean * mean-spev2 / nbr))

    def posterior(self,name):

        for desc in self.cli_desc:
            if name in self.cli_desc[desc]:
                potential = gum.Potential().add(self.ttbn.variable(name))
                tenseur = margSumIn(name,self.phi[desc],self.cli_desc[desc],self.max_tt_rank,self.precision)
                potential[:] = tenseur.full().data
                #normalized_pot = potential[:]

                #if min(normalized_pot) < 0:
                #    new_pot = [normalized_pot[i]+abs(min(normalized_pot)) for i in range(len(normalized_pot))]
                #    potential[:] = new_pot

                #normalized_pot = potential.normalize()[:]
                
                #return normalized_pot
                return potential.normalize()
                
