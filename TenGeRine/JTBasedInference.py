#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 16:23:32 2020

@author: gaspard
"""

import pyAgrum as gum
import numpy as np

class JTBasedInference:
    def __init__(self, bn, root=None, evidence=dict(), target=None):
        self.bn = bn
        gen = gum.JunctionTreeGenerator()
        self.jt = gen.junctionTree(bn)
        self.root = root
        self.setEvidence(evidence)
        self.target = target
        self.psi = dict()
        self.phi = dict()
        self.sep_desc = dict()
        self.cli_desc = dict()
        self.marginales = dict()

    def addTarget(self,target):
        if self.target is not None:
            raise ValueError(f"A target already exists : {self.target}")

        if isinstance(target,str):
            self.target = self.bn.idFromName(target)
        elif isinstance(target,int):
            self.target = target
        else :
            raise ValueError(f"Target should be an id or a name")

    def _updateValue(self,key,value):
        """
        Transform the value into the according potential
        """

        var = self.bn.variable(key)
        labels = list(var.labels())

        if isinstance(value,int):

            if str(value) in labels:
                idx = labels.index(str(value))
                array = np.zeros(len(labels))
                array[idx] = 1

                return gum.Potential().add(var).fillWith(array)
            else:
                raise ValueError(f"{value}")
        elif isinstance(value,str):
            idx = labels.index(value)
            array = np.zeros(len(labels))
            array[idx] = 1
            return gum.Potential().add(var).fillWith(array)
        elif isinstance(value,list):
            return gum.Potential().add(var).fillWith(value).normalize()
        else:
            raise TypeError(f"_updateValue parameter must be an int, a str or a list, not {type(value)}")

    def addEvidence(self,key,value):
        """
        Adds a new evidence on a node (might be soft or hard).

        Parameters
        ----------
        key : int or str
          a node id or name
        val :
          (int) a node value
        val :
          (str) the label of the node value
        vals : list
          a list of values
        """

        if isinstance(key,int):
            key = self._IdToName(key)

        if key in self.evidence and self.evidence[key] is not None:
            raise ValueError(f"An evidence already exists : {key} equals {value}. Please use chgEvidence()")

        if isinstance(key,str):    
            self.evidence[key] = self._updateValue(key,value)
        else:
            raise TypeError(f"addEvidence parameter must be an int (id) or a str (name), not {type(evidces)}")

    def chgEvidence(self,key,value):
        """
        Change the value of an already existing evidence on a node (might be soft or hard).

        Parameters
        ----------
        key : int or str
          a node id or name
        val :
          (int) a node value
        val :
          (str) the label of the node value
        vals : list
          a list of values
        """
        if not key in self.evidence:
            raise ValueError(f"No evidence on {key}")
        if isinstance(key,str):    
            self.evidence[key] = self._updateValue(key,value)
        elif isinstance(key,int):
            self.evidence[self._IdToName(key)] = self._updateValue(self._IdToName(key),value)
        else:
            raise TypeError(f"addEvidence parameter must be an int (id) or a str (name), not {type(key)}")

    def setEvidence(self, evidces):
        """
        Erase all the evidences and apply addEvidence(key,value) for every pairs in evidces.

        Parameters
        ----------
        evidces : dict
          a dict of evidences

        Raises
        ------
        gum.InvalidArgument
            If one value is not a value for the node
        gum.InvalidArgument
            If the size of a value is different from the domain side of the node
        gum.FatalError
            If one value is a vector of 0s
        gum.UndefinedElement
            If one node does not belong to the Bayesian network
        """
        if not isinstance(evidces, dict):
            raise TypeError(f"setEvidence parameter must be a dict, not {type(evidces)}")
        self.eraseAllEvidence()
        for k,v in evidces.items():
            self.addEvidence(k,v)

    def eraseAllEvidence(self):
        """
        Erase all the evidences
        """
        self.evidence = dict()

    def updateEvidence(self, evidces):
        """
        Apply chgEvidence(key,value) for every pairs in evidces (or addEvidence).

        Parameters
        ----------
        evidces : dict
          a dict of evidences

        Raises
        ------
        gum.InvalidArgument
            If one value is not a value for the node
        gum.InvalidArgument
            If the size of a value is different from the domain side of the node
        gum.FatalError
            If one value is a vector of 0s
        gum.UndefinedElement
            If one node does not belong to the Bayesian network
        """
        if not isinstance(evidces, dict):
            raise TypeError(f"setEvidence parameter must be a dict, not {type(evidces)}")

        for k,v in evidces.items():
            if self.hasEvidence(k):
                self.chgEvidence(k,v)
            else:
                self.addEvidence(k,v)

    def hasEvidence(self, key):
        return key in self.evidence

    def _IdToName(self,id):
        return self.bn.variable(id).name()