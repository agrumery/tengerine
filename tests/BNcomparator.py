#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 14:04:03 2020

@author: gaspard
"""

import pyAgrum as gum
import pyAgrum.lib.dynamicBN as gdyn
import math
import sys
import TenGeRine as tgr
import numpy as np
import argparse
import math
import time
from datetime import datetime
import csv
import os
import json

def errorsfull(marginales_SS,marginales_SSTT):
    nberror = 0
    maxerror = 0

    for key_SS in marginales_SS:
        for marginale_SSTT in marginales_SSTT[key_SS]:
            diff = max(abs(m) for m in (marginales_SS[key_SS][:]-marginale_SSTT))
            if diff > 0:
                nberror+=1
                if diff > maxerror:
                    maxerror = diff

    return nberror, maxerror

def errorsMaxMeanStd(marginales_SS,marginales_SSTT):
    errors = []
    maxdiff = 0
    worst_case = []
    name = ''

    for key_SS in marginales_SS:
        for marginale_SSTT in marginales_SSTT[key_SS]:

            for i in range(len(marginales_SS[key_SS])):
                diff = [abs(m) for m in (marginales_SS[key_SS][i]-marginale_SSTT)]
                errors+=diff
                if max(diff) > maxdiff:
                    maxdiff = max(diff)
                    worst_case = [marginales_SS[key_SS][i],marginale_SSTT]
                    name = key_SS

    return np.max(errors),np.mean(errors),np.std(errors), worst_case, name

def errorsMaxMeanStdRelatif(marginales_SS,marginales_SSTT):
    errors = []
    maxdiff = 0
    worst_case = ""
    name = ''
    for key_SS in marginales_SS:
        for marginale_SSTT in marginales_SSTT[key_SS]:
            for marginale_SS in marginales_SS[key_SS]:
                tab = []
                for i in range(len(marginale_SSTT)):
                    if marginale_SS[i]!=0:
                        diff = abs(marginale_SS[i] - marginale_SSTT[i])/marginale_SS[i]
                        if diff > maxdiff:
                            maxdiff = diff
                            worst_case = [marginale_SSTT,marginale_SS]
                            name = key_SS
                        tab.append(diff)
                errors += tab

    return np.max(errors),np.mean(errors),np.std(errors), worst_case, name

def errorsMaxMeanStdRelatifFull(marginales_SS,marginales_SSTT):
    errors = []
    maxdiff = 0
    worst_case = []
    name = ''
    for key_SS in marginales_SS:
        for i in range(len(marginales_SSTT[key_SS])):
            marginale_SS = marginales_SS[key_SS][i]
            marginale_SSTT = marginales_SSTT[key_SS][i]
            tab = []

            for j in range(len(marginale_SS)):
                if marginale_SS[j]!=0:
                    diff = abs(marginale_SSTT[j] - marginale_SS[j])/marginale_SS[j]
                    if diff > maxdiff:
                        maxdiff = diff
                        worst_case = [marginale_SSTT,marginale_SS]
                        name = key_SS
                    tab.append(diff)
            errors += tab

    return np.max(errors),np.mean(errors),np.std(errors), worst_case, name

def errorsMaxMeanStdRelatifBN(bn,marginales_SSTT):
    errors = []
    maxdiff = 0
    worst_case = []
    name = ''
    ie = gum.LazyPropagation(bn)
    ie.makeInference

    for key in [bn.variable(n).name() for n in bn.nodes()]:
        for marginale_SSTT in marginales_SSTT[key]:
            for marginale_SS in [ie.posterior(key)]:
                tab = []
                for i in range(len(liste)):
                    if marginale_SS[i]!=0:
                        diff = abs(marginale_SS[i] - marginale_SSTT[i])/marginale_SS[i]
                        if diff > maxdiff:
                            maxdiff = diff
                            worst_case = [marginale_SSTT,marginale_SS]
                            name = key
                        tab.append(diff)
                errors += tab

    return np.max(errors),np.mean(errors),np.std(errors), worst_case, name

def hasNaN(marginales):
    for elt in marginales:
        for x in marginales[elt]:
            for val in x:
                if math.isnan(val):
                    return True
    return False

def hasNaN2(marginales):
    for elt in marginales:
        for x in marginales[elt]:
            for val in x:
                if math.isnan(val):
                    return elt, True
    return False

def hasNegativeValues(marginales):
    for elt in marginales:
        for x in marginales[elt]:
            for val in x:
                if val < 0:
                    return elt, True
    return False
            
def compareBN(bns,max_tt_rank,precisions,algos,info=False,SS=True,SSTT=True,save=True,full=False,path_to_save='./'):
    res = dict()
    now = datetime.now()
    epsilon = str(precision).replace(',', '.')

    marginales_SS = []
    marginales_SSTT = []

    for name in bns:
        bn = gum.loadBN('../resources/PGMrepository/bif/'+name+'.bif')

        ie = tgr.ShaferShenoyTensorTrain(bn,max_tt_rank=max_tt_rank,precision=precision)
        marginales_SSTT, tempsSSTT, nb_param_SSTT = ie.makeInference(full=full,withDetails=True,info=info)

        print( f"Durée inférence ShaferShenoyTensorTrain : {sum(tempsSSTT.values()):.2f} secondes \n NB param : {nb_param_SSTT}")

        if SS:
            ie = tgr.ShaferShenoy(bn)
            marginales_SS, tempsSS, nb_param_SS = ie.makeInference(full=full,withDetails=True,info=info)
            print( f"Durée inférence ShaferShenoy : {sum(tempsSS.values()):.2f} secondes \n NB param : {nb_param_SS}")

        key = name+'_'+str(epsilon)+"_SS"
        key2 = name+'_'+str(epsilon)+"_SSTT"

        if SS:
            blob = dict()
            blob['BN name']=name
            blob['Type']='SS'
            blob['Precision'] = precision
            blob['Temps_pretreat'] = tempsSS['Pre treatment']
            blob['Temps_offline'] = tempsSS['Initialization']
            blob['Temps_collect'] = tempsSS['Collection phase']
            blob['Temps_distrib'] = tempsSS['Distribution phase']
            blob['Temps_update_pot'] = tempsSS['Potential update']
            blob['Temps_marginales'] = tempsSS['Posterior generation']
            blob['Temps_total'] = sum(tempsSS.values())
            blob['Temps_algo'] = tempsSS['Pre treatment']+tempsSS['Initialization']+tempsSS['Collection phase']+tempsSS['Distribution phase']
            blob['Max_abs'] = 0
            blob['Mean_abs'] = 0
            blob['STD_abs'] = 0
            blob['Max_rel'] = 0
            blob['Mean_rel'] = 0
            blob['STD_rel'] = 0
            blob['Nb_Param'] = nb_param_SS
            res[key] = blob

        if SSTT:
            blob = dict()
            blob['DBN number']=name
            blob['Type']='SSTT'
            blob['Precision'] = precision
            blob['Temps_pretreat'] = tempsSSTT['Pre treatment']
            blob['Temps_offline'] = tempsSSTT['Initialization']
            blob['Temps_collect'] = tempsSSTT['Collection phase']
            blob['Temps_distrib'] = tempsSSTT['Distribution phase']
            blob['Temps_update_pot'] = tempsSSTT['Potential update']
            blob['Temps_marginales'] = tempsSSTT['Posterior generation']
            blob['Temps_total'] = sum(tempsSSTT.values())
            blob['Temps_algo'] = tempsSSTT['Pre treatment']+tempsSSTT['Initialization']+tempsSSTT['Collection phase']+tempsSSTT['Distribution phase']

            if SS:
                errors_abs = errorsMaxMeanStd(marginales_SS,marginales_SSTT)
                blob['Max_abs'] = errors_abs[0]
                blob['Mean_abs'] = errors_abs[1]
                blob['STD_abs'] = errors_abs[2]
                errors_rel = errorsMaxMeanStdRelatifFull(marginales_SS,marginales_SSTT)
                blob['Max_rel'] = errors_rel[0]
                blob['Mean_rel'] = errors_rel[1]
                blob['STD_rel'] = errors_rel[2]
            blob['Nb_Param'] = nb_param_SSTT
            res[key2] = blob

        for algo in algos:
            keyalgo = name+'_'+str(epsilon)+"_"+algo
            t0 = time.time()

            if algo == '':
                break
            if algo == 'LoopyBeliefPropagation':
                ie = gum.LoopyBeliefPropagation(bn)
                ie.makeInference()

            marginales = dict()
        
            for node in bn.nodes():
                marginales[bn.variable(node).name()] = [ie.posterior(node)[:]] 

            blob = dict()
            blob['DBN number']=name
            blob['Type']=algo
            blob['Precision'] = precision
            blob['Temps_pretreat'] = '-'
            blob['Temps_offline'] = '-'
            blob['Temps_collect'] = '-'
            blob['Temps_distrib'] = '-'
            blob['Temps_update_pot'] = '-'
            blob['Temps_marginales'] = '-'
            blob['Temps_total'] = time.time()-t0
            blob['Temps_algo'] = '-'

            if SS:
                errors_abs = errorsMaxMeanStd(marginales_SS,marginales)
                blob['Max_abs'] = errors_abs[0]
                blob['Mean_abs'] = errors_abs[1]
                blob['STD_abs'] = errors_abs[2]
                errors_rel = errorsMaxMeanStdRelatifFull(marginales_SS,marginales)
                blob['Max_rel'] = errors_rel[0]
                blob['Mean_rel'] = errors_rel[1]
                blob['STD_rel'] = errors_rel[2]
            blob['Nb_Param'] = '-'
            res[keyalgo] = blob

        if SS:
            print(f'{name} => end of the comparaison of SS, SSTT (epsilon : {epsilon}) and {algo}')
        else:
            print(f'{name} => end of the comparaison of SSTT (epsilon : {epsilon}) and {algo}')
        print()

    if save:
        dt_string = now.strftime("%d_%m_%Y")
        time_string = now.strftime("%H_%M_%S")
        new_path = path_to_save+dt_string

        if not(os.path.exists(new_path)):
            os.mkdir(new_path)
        
        print(bns)
        if len(bns) > 1:
            name = "multiple"

        with open(new_path+'/'+name+'_'+time_string+'.csv', 'w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

            listeKey = res[list(res.keys())[0]].keys()
            filewriter.writerow(listeKey)

            for key in res:
                blobl = []
                for truc in res[key]:
                    blobl.append(res[key][truc])

                filewriter.writerow(blobl)
                
    return marginales_SS, marginales_SSTT

def parseArguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("bns", help="paths of bns", type=str)
    
    parser.add_argument("-a","--algo",help="Optional algorithms",type=str,default='')
    parser.add_argument("-mr", "--maxrank", help="TT maximum rank", type=int, default=np.iinfo(np.int32).max)
    parser.add_argument("-p", "--precision", help="TT precision", type=float, default=0.001)
    parser.add_argument("-f", "--full", help="Compute all posteriors in all cliques", type=str, default='True')
    parser.add_argument("-ss", "--SS", help="Compute SS values", type=str, default='True')
    parser.add_argument("-sstt", "--SSTT", help="Compute SSTT values", type=str, default='True')
    parser.add_argument("-i", "--info", help="Show info", type=str, default='True')
    parser.add_argument("-path","--path",help="Path of saved file",type=str,default='./')
    parser.add_argument("-sf", "--save", help="Save results", type=str, default='True')

    args = parser.parse_args()
    return args

if __name__ == "__main__":

    args = parseArguments()

    bns = args.bns.split(',')

    for n in bns:
        try:
            bn = gum.loadBN('../resources/PGMrepository/bif/'+n+'.bif')
        except:
            raise ValueError('Unknown path')

    max_tt_rank = args.maxrank
    precision = args.precision

    SS = args.SS == 'True'
    SSTT = args.SSTT == 'True'
    info = args.info == 'True'
    save = args.save == 'True'
    full = args.full == 'True'
    path_to_save = args.path

    algos = args.algo.split(',')

    compareBN(bns,max_tt_rank,precision,algos,info,SS,SSTT,save,full,path_to_save)

    #python BNcomparator.py 'alarm,asia,Barley,carpo,child,Diabetes,hailfinder,insurance,Mildew,Munin1,Pigs,Water' -p 1e-5
