import unittest
import pyAgrum as gum
import numpy as np
import sys
import os
import random
import json

root_tengerine=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(root_tengerine)

import tensorflow as tf
tf.get_logger().setLevel('ERROR')

import TenGeRine as ttgum

class Test_TTInference(unittest.TestCase):

  """
  Disabling test since feature is experimental

  def test_hybridInference(self):
    bn = gum.loadBN(root_tengerine+'/resources/PGMrepository/bif/asia.bif')
    
    exa_ie = ttgum.ShaferShenoy(bn,info=False)
    exa_mar,_,_ = exa_ie.makeInference()
    exa_pev=exa_ie.getProbabilityOfEvidence()

    exa_ie = ttgum.ShaferShenoy(bn,root=1,info=False)
    _,exa_pev_direct,_ = exa_ie.probabilityOfEvidence()

    self.assertEqual(exa_pev,exa_pev_direct)

    jt = exa_ie.jt
    max_size = max([len(jt.clique(node)) for node in jt.nodes()])

    for i in range(1,max_size+1):
      ie_hybrid_ss = ttgum.HybridShaferShenoy(bn,precision=1e-2,info=False,max_dim=i)
      marginales_hybrid,_,_ = ie_hybrid_ss.makeInference()
      hybrid_ss_pev,var = ie_hybrid_ss.getProbabilityOfEvidence(with_stats=True)
      print(hybrid_ss_pev,var)
      
      self.assertAlmostEqual(exa_pev,hybrid_ss_pev,delta=1e-3)
      self.assertLessEqual(var,1e-7)

      p = ie_hybrid_ss.getProbabilityOfEvidence(with_stats=False)
      self.assertAlmostEqual(exa_pev,p,delta=1e-3)
  """

  def test_posterior(self):

    models = ['asia','alarm']
    precision = 1e-5
    nb_inf = 5

    # On limite à une évidence pour pas s'embêter avec les évidences incompatibles
    nb_evidence = 1

    for name in models:

      model = gum.loadBN(root_tengerine+'/resources/PGMrepository/bif/'+name+'.bif')

      for i in range(nb_inf):
          ie_gum = gum.LazyPropagation(model)
          ie_ex = ttgum.ShaferShenoy(model)
          ie_app = ttgum.ShaferShenoyTensorTrain(model,precision=precision)

          evs = random.sample(list(model.names()),nb_evidence)
          evidences = dict()

          for e in evs:
              value = random.sample(list(model.variable(e).labels()),1)[0]
              ie_gum.addEvidence(e,value)
              ie_ex.addEvidence(e,value)
              ie_app.addEvidence(e,value)
              evidences[e] = value

          ie_gum.makeInference()
          ie_ex.makeInference()
          ie_app.makeInference()

          for var in list(model.names()):

              true_post = ie_gum.posterior(var) 
              ex_post = ie_ex.posterior(var)
              app_post = ie_app.posterior(var)

              try:
                np.testing.assert_almost_equal(true_post[:],ex_post[:],7)
                np.testing.assert_almost_equal(true_post[:],app_post[:],3)
              except AssertionError as err:
                print(f"Error with {name} and evidences : {json.dumps(evidences)}")
                print(err)
                raise err              

  def test_probabilityOfNoEvidence(self):
    bn = gum.loadBN(root_tengerine+'/resources/PGMrepository/bif/asia.bif')

    # Precision based approximation
    ttbn = ttgum.TTBN(bn,precision=1e-2)

    # Complete Inference
    exa_ie = ttgum.ShaferShenoy(bn)
    exa_ie.makeInference()
    exa_pev = exa_ie.getProbabilityOfEvidence()

    # Collect phase only (root needed)
    exa_ie = ttgum.ShaferShenoy(bn,root=1)
    exa_pev_direct = exa_ie.probabilityOfEvidence()

    self.assertEqual(exa_pev,exa_pev_direct)

    # Complete Approximate Inference
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,precision=1e-3)
    tt_ie.makeInference()
    tt_pev,var =tt_ie.getProbabilityOfEvidence(with_stats=True)
    
    self.assertAlmostEqual(exa_pev,tt_pev,delta=1e-3)
    self.assertLessEqual(var,1e-7)

    # Approximate Collect phase only (root needed)
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,root=1,precision=1e-2)
    tt_pev_direct = tt_ie.probabilityOfEvidence()
    
    self.assertAlmostEqual(exa_pev,tt_pev_direct,delta=1e-3)

  def test_probabilityOfEvidence(self):
    bn = gum.loadBN(root_tengerine + '/resources/PGMrepository/bif/asia.bif')
    
    ###############
    evs={"visit_to_Asia?":[0,1]}

    # Precision based approximation
    ttbn = ttgum.TTBN(bn,precision=1e-2)

    # Complete Inference
    exa_ie = ttgum.ShaferShenoy(bn,evidence=evs)
    exa_ie.makeInference()
    exa_pev=exa_ie.getProbabilityOfEvidence()

    # Complete Approx Inference
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,precision=1e-2,evidence=evs)
    tt_ie.makeInference()
    tt_pev,stdev = tt_ie.getProbabilityOfEvidence(with_stats=True)

    self.assertAlmostEqual(exa_pev,tt_pev,delta=1e-3)
    self.assertLessEqual(stdev,1e-7)

    # Approximate Collect phase only (root needed)
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,root=1,evidence=evs,precision=1e-2)
    tt_pev_direct = tt_ie.probabilityOfEvidence()
    
    self.assertAlmostEqual(exa_pev,tt_pev_direct,delta=1e-3)

    ###############
    evs={"visit_to_Asia?":[1,0]}

    # Complete Inference
    exa_ie = ttgum.ShaferShenoy(bn,evidence=evs)
    exa_ie.makeInference()
    exa_pev=exa_ie.getProbabilityOfEvidence()

    # Complete Approx Inference
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,precision=1e-2,evidence=evs)
    tt_ie.makeInference()
    tt_pev,stdev = tt_ie.getProbabilityOfEvidence(with_stats=True)

    self.assertAlmostEqual(exa_pev,tt_pev,delta=1e-3)
    self.assertLessEqual(stdev,1e-7)

    # Approximate Collect phase only (root needed)
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,root=1,evidence=evs,precision=1e-2)
    tt_pev_direct = tt_ie.probabilityOfEvidence()
    
    self.assertAlmostEqual(exa_pev,tt_pev_direct,delta=1e-3)

    ###############
    evs={"visit_to_Asia?":[0.3,0.7],'smoking?':[0.8,0.4]}

    # Complete Inference
    exa_ie = ttgum.ShaferShenoy(bn,evidence=evs)
    exa_ie.makeInference()
    exa_pev=exa_ie.getProbabilityOfEvidence()

    # Complete Approx Inference
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,precision=1e-2,evidence=evs)
    tt_ie.makeInference()
    tt_pev,stdev = tt_ie.getProbabilityOfEvidence(with_stats=True)

    self.assertAlmostEqual(exa_pev,tt_pev,delta=1e-3)
    self.assertLessEqual(stdev,1e-7)

    # Approximate Collect phase only (root needed)
    tt_ie = ttgum.ShaferShenoyTensorTrain(bn,root=1,evidence=evs,precision=1e-2)
    tt_pev_direct = tt_ie.probabilityOfEvidence()
    
    self.assertAlmostEqual(exa_pev,tt_pev_direct,delta=1e-3)
