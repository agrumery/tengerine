import sys
import os
from pathlib import Path
from timeit import default_timer as timer

import pyAgrum as gum


def _tryToRead(dirname, filename,exclude):
  start1 = 0
  start2 = 0
  end1 = -1
  end2 = -1

  try:
    start1 = timer()
    bn = gum.loadBN(f"{dirname}/{filename}")
    end1 = timer()

    if filename not in exclude:
      start2 = timer()
      ie = gum.LazyPropagation(bn)
      ie.makeInference()
      end2 = timer()
  except:
    pass

  return f"{bn.size():4} nodes, {bn.sizeArcs():4} arcs", end1 - start1, end2 - start2


def tryToRead(dirname, filename,nbiter,exclude=[]):
  sum1 = 0
  nb1 = 0
  sum2 = 0
  nb2 = 0

  print(f"  - {filename:30} [", flush=True, end='')

  for i in range(nbiter):
    print(".", flush=True, end='')
    msg, t1, t2=_tryToRead(dirname, filename,exclude)
    if t1 == -1:
      continue
    sum1 += t1
    nb1 += 1
    if t2 == -1:
      continue
    sum2 += t2
    nb2 += 2

  print(f"] :  \t{msg}\t", end='')
  if nb1 > 0:
    print(f"{1000*(sum1/nb1):8.1f} ms \t", end='')
  else:
    print(f"{-1:8.1f}    \t", end='')
  if nb2 > 0:
    print(f"{1000*(sum2/nb2):8.1f} ms")
  else:
    print(f"{-1:8.1f}")


nbiter = 5
print(f"{' file ':*^{30+7+nbiter}}" + "  \t"
      + f"{' structure ':*^21}" + "\t"
      + f"{' load ':*^12}" + "\t"
      + f"{' LazyP ':*^12}")

if len(sys.argv) == 1:
  p=Path(".")
  for dirName, _, filenames in sorted(os.walk(p)):
    if dirName == ".":
      continue
    if not dirName.startswith(('./.', '.\\.')):
      print(f"+ {dirName}")
      for filename in sorted(filenames):
        tryToRead(dirName, filename,nbiter,exclude=['main4.net'])
else:
  filename=sys.argv[1]
  if "." not in filename:
    print("Filename f{filename} needs a suffix !")
  else:
    suffix=filename.split('.')[-1]
    longfilename=f"./{suffix}/{filename}"
    if not Path(longfilename).exists():
      print(f"File {filename} does not exist !")
    else:
      tryToRead(suffix, filename,nbiter,exclude=['main4.net'])
