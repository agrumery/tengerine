from setuptools import setup, find_packages

setup(name='TenGeRine',
      version='0.4',
      description='Tensor Train based inference',
      url='https://gitlab.com/agrumery/TenGeRine',
      author='Gaspard Ducamp',
      packages=find_packages(),
      install_requires=[
            'numpy==1.18',
            'tensorflow',
            'pyAgrum',
            'pydotplus',
            'matplotlib',
            'scipy',
            'tensap'
      ],
      zip_safe=False)
