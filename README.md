# TenGeRine

Using Tensor-Train format to approximate potential during inferences.

## Requirements

Requirements :  
- numpy (1.18)
- tensorflow
- pyAgrum
- pydotplus
- matplotlib
- scipy
- tensap
- IPython

## Installation

1. `$> git clone https://gitlab.com/agrumery/tengerine.git`
2. `$> cd tengerine`
3. `$> pip install .`

## Example 

~~~~
    import pyAgrum as gum
    import pyAgrum.lib.notebook as gnb
    import TenGeRine as ttgum
    
    import numpy as np

    ############################################################
    #                   Testing BN2TTBN                        #
    ############################################################

    bn = gum.loadBN('tengerine-devel/resources/PGMRepository/bif/asia.bif')

    # Precision based approximation
    ttbn = ttgum.TTBN(bn,precision=1e-4)

    ############################################################
    #                        Testing BN                        #
    ############################################################
    bn = gum.loadBN('tengerine-devel/resources/PGMRepository/bif/asia.bif')

    ie_ex = ttgum.ShaferShenoy(bn)
    ie_ex.makeInference()

    ie_app = ttgum.ShaferShenoyTensorTrain(bn,precision=1e-4)
    ie_app.makeInference()

    for node in bn.names():
        np.testing.assert_almost_equal(ie_ex.posterior(node)[:],ie_app.posterior(node)[:],4)
        
    gnb.sideBySide(*[ie_ex.posterior(node) for node in list(bn.names())[:4]])
    gnb.sideBySide(*[ie_app.posterior(node) for node in list(bn.names())[:4]])

    gnb.sideBySide(*[ie_ex.posterior(node) for node in list(bn.names())[5:]])
    gnb.sideBySide(*[ie_app.posterior(node) for node in list(bn.names())[5:]])

    ie_ex.addTarget('tuberculos_or_cancer?')
    ie_app.addTarget('tuberculos_or_cancer?')

    ie_ex.addEvidence('tuberculos_or_cancer?','c2')
    ie_app.addEvidence('tuberculos_or_cancer?','c2')

    poe_ex = ie_ex.probabilityOfEvidence()
    poe_app = ie_app.probabilityOfEvidence()

    np.testing.assert_almost_equal(poe_ex,poe_app,4)
~~~~

### Exact Inferences :

- Shafer Shenoy
    - Complete Inference
    - ProbabilityOfEvidence (collect phase to choosed root)

### Tensor Train based Inferences :

- Shafer Shenoy
    - Complete Inference
    - ProbabilityOfEvidence (collect phase to choosed root)

### Related bibliography

- pyAgrum : G. Ducamp, Ch. Gonzales, P.‑H. Wuillemin : “aGrUM/pyAgrum : a toolbox to build models and algorithms for Probabilistic Graphical Models in Python”, 10th International Conference on Probabilistic Graphical Models, vol. 138, Proceedings of Machine Learning Research, Skørping, Denmark, pp. 609-612 (2020)
- Tengerine : G. Ducamp, Ph. Bonnard, A. Nouy, P.‑H. Wuillemin : “An Efficient Low-Rank Tensors Representation for Algorithms in Complex Probabilistic Graphical Models”, 10th International Conference on Probabilistic Graphical Models, vol. 138, Proceedings of Machine Learning Research, Skørping, Denmark, pp. 173-184 (2020)